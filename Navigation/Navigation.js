// Navigation/Navigation.js
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import ListPlayers from '../Components/ListPlayers'
import PlayerDetail from '../Components/PlayerDetail'

const MainNavigator = createStackNavigator({
    Listing: {
        screen: ListPlayers,
        navigationOptions: {
            title: 'Liste des joueurs'
        }
    },
    Detail: {
        screen: PlayerDetail
    }
})

export default createAppContainer(MainNavigator)