// API/Players.js

export function getAllPlayers () {
  const url = 'https://eurosportdigital.github.io/eurosport-react-native-developer-recruitment/docs/headtohead.json'
  return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.error(error))
}