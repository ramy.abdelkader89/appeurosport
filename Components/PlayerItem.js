// Components/PlayerItem.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

class PlayerItem extends React.Component {    

    render() {
        const player = this.props.player;
        const displayDetailPlayer = this.props.displayDetailPlayer

        return (
            <TouchableOpacity style={styles.main_container} onPress={() => displayDetailPlayer(player)}>
                <Image
                    style={styles.image}
                    source={{ uri: player.picture }}
                />
                <View style={styles.content_container}>
                    <View style={styles.header_container}>
                        <Text style={styles.title_text}>{player.shortname}</Text>
                        <Image
                            style={styles.imageCountry}
                            source={{ uri: player.country.picture }}
                        />
                    </View>
                    <View style={styles.description_container}>
                        <Text style={styles.player_country}>{player.country.code}</Text>
                        <Text style={styles.player_details}>{player.data.age + " ans"}</Text>
                        <Text style={styles.player_details}>{player.data.height + " cm"}</Text>
                        <Text style={styles.player_details}>{(player.data.weight / 1000) + " kg"}</Text>
                        <Text style={styles.player_details}>{player.data.points + " pts"}</Text>
                    </View>
                    <View style={styles.rank_container}>
                        <Text style={styles.player_rank}>{player.data.rank + "' cla."}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

// Style
const styles = StyleSheet.create({
    main_container: {
        flexDirection: 'row',
        borderBottomColor: '#a9a9a9',
        borderBottomWidth: 1,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: 'rgba(0,0,0,0.3)'
    },
    image: {
        width: 120,
        margin: 5,
        resizeMode: "contain"
    },
    content_container: {
        flex: 1,
        margin: 5,
    },
    header_container: {
        flex: 1,
        flexDirection: 'row'
    },
    imageCountry: {
        width: 30,
        height: 20,
        resizeMode: "cover"
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5,
        color: "#fff"
    },
    description_container: {
        flex: 5
    },
    player_country: {
        fontStyle: 'italic',
        color: '#ddd',
        marginBottom: 5
    },
    player_details: {
        fontSize: 13,
        color: "#fff",
        marginTop: 7,
        lineHeight:13
    },
    rank_container: {
        flex: 1
    },
    player_rank: {
        textAlign: 'right',
        fontSize: 16,
        fontWeight: 'bold',
        color: "#000"
    }
})

export default PlayerItem