// Components/ListPlayers.js

import React from 'react'
import { StyleSheet, View, FlatList, ActivityIndicator } from 'react-native'
import { getAllPlayers } from '../API/Players'
import PlayerItem from './PlayerItem'


class ListPlayers extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = { isLoading: true, players: [] }
    }
    
    componentDidMount(){
        getAllPlayers().then(data => {
            this.setState({ isLoading: false, players: data.players })
        })
    }

    _displayDetailPlayer = (data) => {
        this.props.navigation.navigate("Detail", { data: data })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#0c9"/>
                </View>
            )
        }
        return (
            <View style={styles.main_container}>                
                <FlatList
                    data={this.state.players}
                    keyExtractor={(item) => item.data.rank.toString()}
                    renderItem={({item}) => <PlayerItem player={item} displayDetailPlayer={this._displayDetailPlayer} />}
                />
            </View>
        )
    }
}

// Style
const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.3)'
    },
    title: {
        textAlign: 'center',
        backgroundColor: "rgba(0,0,0,0.8)",
        color: "#ef202d",
        fontWeight: 'bold',
        fontSize: 20,
        paddingTop: 30,
        paddingBottom: 10,
        borderBottomColor: '#a9a9a9',
        borderBottomWidth: 2
    },
})

export default ListPlayers