// Components/PlayerDetail.js

import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'

class PlayerDetail extends React.Component {

    render() {
        const player = this.props.navigation.state.params.data;
        const win = player.data.last.filter(it => it == 1);
        const lose = player.data.last.filter(it => it == 0);
        
        return (
            <View style={styles.main_container}>
                <Image
                    style={styles.image}
                    source={{ uri: player.picture }}
                />
                <View style={styles.content_container}>
                    <Text style={styles.title_text}>{player.firstname+" "+player.lastname}</Text>
                    <View style={styles.country_container}>
                        <Image
                            style={styles.imageCountry}
                            source={{ uri: player.country.picture }}
                        />
                        <Text style={styles.player_country}>{player.country.code}</Text>
                    </View>
                    <View style={styles.description_container}>
                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Age :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{player.data.age + " ans"}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>sex :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{player.sex}</Text>
                                </View>
                            </View>
                        </View>                            
                        
                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Taille :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{player.data.height + " cm"}</Text>
                                </View>
                            </View>
                        </View>
                        
                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Poids :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{(player.data.weight / 1000) + " kg"}</Text>
                                </View>
                            </View>
                        </View>
                        
                        <View style={styles.player_stat}>                                
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Points :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{player.data.points + " pts"}</Text>
                                </View>
                            </View>
                        </View>
                        
                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Classement :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{player.data.rank+"'" }</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Victoire :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{win.length}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.player_stat}>
                            <View style={styles.player_stat_container}>
                                <View style={styles.player_stat_container_row1}>
                                    <Text style={styles.player_details}>Défaite :</Text>
                                </View>
                                <View style={styles.player_stat_container_row2}>
                                    <Text style={styles.player_details}>{lose.length}</Text>
                                </View>
                            </View>
                        </View>                        
                    </View>
                </View>
            </View>
        )
    }
}

// Style
const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: 'rgba(0,0,0,0.3)'
    },
    image: {
        width: 120,
        height:200,
        margin: 5,
        resizeMode: "contain"
    },
    content_container: {
        flex: 1,
        margin: 5,
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 23,
        marginBottom: 5,
        color: "#fff"
    },
    country_container: {
        flex: 1,
        flexDirection: 'row'
    },
    imageCountry: {
        width: 30,
        height: 20,
        resizeMode: "cover"
    },   
    player_country: {
        fontStyle: 'italic',
        marginLeft: 10,
        color: '#ddd'
    }, 
    description_container: {
        flex: 12
    },    
    player_details: {
        fontSize: 18,
        color: "#fff",
        lineHeight:17
    },
    player_stat:{
        height: 30
    },
    player_stat_container:{ 
        flex: 1, 
        flexDirection: 'row' 
    },
    player_stat_container_row1:{ 
        flex: 3 
    },
    player_stat_container_row2:{ 
        flex: 2 
    }    
})

export default PlayerDetail